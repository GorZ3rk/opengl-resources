The resources i used for OpenGL...

### Libraries

*OpenGL libraries*

- [GLFW](https://www.glfw.org/) - multi-platform library for OpenGL
- [Glew](http://glew.sourceforge.net/) - OpenGL extension wrangler library
- [Glad](https://glad.dav1d.de/) - open-source library
- [std_image.h](https://github.com/nothings/stb/blob/master/stb_image.h) - image loader library
- [glm](https://github.com/g-truc/glm) - OpenGL Mathematics (GLM)

### Videos

*awesome comprehensive OpenGL video tutorials*

- [Jamie King](https://www.youtube.com/playlist?list=PLRwVmtr-pp06qT6ckboaOhnm9FxmzHpbY) - 3D Computer Graphics Using OpenGL
- [The Cherno](https://www.youtube.com/playlist?list=PLlrATfBNZ98foTJPJ_Ev03o2oq3-GGOS2) - OpenGL
- [OGL dev](https://ogldev.org/) - Modern OpenGL tutorials

### Websites

*awesome comprehensive OpenGL non-video tutorials*

- [Learn OpenGL](https://learnopengl.com/) - Modern OpenGL Tutorials
- [Learn OpenGL](https://learnopengl-cn.github.io/) - Modern OpenGL Tutorials (CN)
- [OpenGL-tutorial](https://www.opengl-tutorial.org/) - OpenGL tutorials with different levels
- [Anton's OpenGL](https://antongerdelan.net/opengl/index.html) - Tutorials for some basic points
- [Alexander Overvoorde OpenGL](https://open.gl/introduction) - Comprehensive tutorials
- [OpenGL](https://www.songho.ca/opengl/index.html) - Fundamental OpenGL tutorials & notes

### MISC

*other resources*

- [3d Math Cheatsheet](https://antongerdelan.net/teaching/3dprog1/maths_cheat_sheet.pdf) - linear algebra basic cheatsheet
- [Real-Time Rendering](http://www.realtimerendering.com/) - Great resources for rendering


### Books

*OpenGL tutorial books*

- [OpenGL SuperBible: Comprehensive Tutorial and Reference](https://www.amazon.de/OpenGL-Superbible-Comprehensive-Tutorial-Reference/dp/0672337479/ref=sr_1_1?__mk_de_DE=%C3%85M%C3%85%C5%BD%C3%95%C3%91&dchild=1&keywords=OpenGL+SuperBible%3A+Comprehensive+Tutorial+and+Reference&qid=1621714814&sr=8-1) - OpenGL Bible
- [OpenGL Programming Guide](https://www.amazon.de/OpenGL-Programming-Guide-Official-Learning/dp/0134495497/ref=sr_1_1?__mk_de_DE=%C3%85M%C3%85%C5%BD%C3%95%C3%91&dchild=1&keywords=OpenGL+Programming+Guide&qid=1621714876&sr=8-1) - OpenGL official programming guide
- [OpenGL Shading Language](https://www.amazon.de/OpenGL-Shading-Language-Randi-Rost/dp/0321637631/ref=sr_1_2?__mk_de_DE=%C3%85M%C3%85%C5%BD%C3%95%C3%91&dchild=1&keywords=OpenGL+Shading+Language&qid=1621714954&sr=8-2) - GLSL

### References

*OpenGL reference*

- [OpenGL Programming Guide](https://www.glprogramming.com/red/) - Official guide for learning OpenGL
- [OpenGL Registry](https://www.khronos.org/registry/OpenGL-Refpages/gl4/) - Khronos OpenGL Official References
- [OpenGL 4.3 API Reference](https://www.khronos.org/files/opengl43-quick-reference-card.pdf) - Quick Reference
- [docs.GL](http://docs.gl/) - OpenGL API Documentation
- [OpenGL Wiki](https://www.khronos.org/opengl/wiki/Main_Page) - Official OpenGL Wiki
